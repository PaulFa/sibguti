#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>

#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>

#define uchar unsigned char
#define ushort unsigned short
#define uint unsigned int

/* Структура для регистрации функции перехватчика входящих ip пакетов */
struct nf_hook_ops bundle;

uint *Hook_Func(uint hooknum,
                  struct sk_buff *skb,
                  const struct net_device *in,
                  const struct net_device *out,
                  int (*okfn)(struct sk_buff *)  )
{
    /* Указатель на структуру заголовка протокола ip в пакете */
    struct iphdr *ip;
    /* Указатель на структуру заголовка протокола tcp в пакете */
    struct tcphdr *tcp;

    /* Проверяем что это IP пакет */
    if (skb->protocol == htons(ETH_P_IP))
    {
	/* Сохраняем указатель на структуру заголовка IP */
	ip = (struct iphdr *)skb_network_header(skb);
	/* Проверяем что это IP версии 4 и внутри TCP пакет */
	if (ip->version == 4 && ip->protocol == IPPROTO_TCP)
	{
	    /* Задаем смещение в байтах для указателя на TCP заголовок */
	    /* ip->ihl - длина IP заголовка в 32-битных словах */
	    skb_set_transport_header(skb, ip->ihl * 4);
	    /* Сохраняем указатель на структуру заголовка TCP */
	    tcp = (struct tcphdr *)skb_transport_header(skb);
	    /* Если пакет идет на 22 порт (ssh) */
	    if (tcp->dest == htons(22))
	    {
		    return NF_REJECT;
	    }
	}
    }
    /* Пропускаем дальше все пакеты */
    return NF_ACCEPT;
}

int Init(void)
{
    printk(KERN_INFO "Start module Shifter\n");

    /* Заполняем структуру для регистрации hook функции */
    /* Указываем имя функции, которая будет обрабатывать пакеты */
    bundle.hook = Hook_Func;
    /* Указываем семейство протоколов */
    bundle.pf = PF_INET;
    /* Указываем, в каком месте будет срабатывать функция */
    bundle.hooknum = NF_INET_PRE_ROUTING;
    /* Выставляем самый высокий приоритет для функции */
    bundle.priority = NF_IP_PRI_FIRST;
    /* Регистрируем */
    nf_register_hook(&bundle);
    return 0;
}

void Exit(void)
{
    /* Удаляем из цепочки hook функцию */
    nf_unregister_hook(&bundle);
    printk(KERN_INFO "End module Shifter\n");
}

module_init(Init);
module_exit(Exit);
