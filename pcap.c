#include <pcap.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <errno.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netinet/if_ether.h> 


/* Ethernet addresses are 6 bytes */
#define ETHER_ADDR_LEN	6

#pragma pack(push, 1)
	/* Ethernet header */
	struct sniff_ethernet {
		u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination host address */
		u_char ether_shost[ETHER_ADDR_LEN]; /* Source host address */
		u_short ether_type; /* IP? ARP? RARP? etc */
	};
#pragma pack(pop)

	/* IP header */
#pragma pack(push, 1)
	struct sniff_ip {
		u_char ip_vhl;		/* version << 4 | header length >> 2 */
		u_char ip_tos;		/* type of service */
		u_short ip_len;		/* total length */
		u_short ip_id;		/* identification */
		u_short ip_off;		/* fragment offset field */
	#define IP_RF 0x8000		/* reserved fragment flag */
	#define IP_DF 0x4000		/* dont fragment flag */
	#define IP_MF 0x2000		/* more fragments flag */
	#define IP_OFFMASK 0x1fff	/* mask for fragmenting bits */
		u_char ip_ttl;		/* time to live */
		u_char ip_p;		/* protocol */
		u_short ip_sum;		/* checksum */
		struct in_addr ip_src,ip_dst; /* source and dest address */
	};
#pragma pack(pop)
	#define IP_HL(ip)		(((ip)->ip_vhl) & 0x0f)
	#define IP_V(ip)		(((ip)->ip_vhl) >> 4)



/* ethernet headers are always exactly 14 bytes */
#define SIZE_ETHERNET 14

	const struct sniff_ethernet *ethernet; /* The ethernet header */
	const struct sniff_ip *ip; /* The IP header */
	const struct sniff_tcp *tcp; /* The TCP header */
	const char *payload; /* Packet payload */

	u_int size_ip;
	u_int size_tcp;


void my_callback(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* 
	packet) 
{ 
	static int count = 1; 
	fprintf(stdout, "%3d, ", count);
	fflush(stdout);
	count++; 
}

void another_callback(u_char *arg, const struct pcap_pkthdr* pkthdr, 
	const u_char* packet) 
{ 
	int i=0; 
	static int count=0; 
	
	ethernet = (struct sniff_ethernet*)(packet);
		
	printf("Source MAC: ");
	for (i=0; i<ETHER_ADDR_LEN; i++)
	{
		printf("%x", ethernet->ether_shost[i]);
	}
	printf("\n");
	
	printf("Destination MAC: ");
	for (i=0; i<ETHER_ADDR_LEN; i++)
	{
		printf("%x", ethernet->ether_dhost[i]);
	}
	printf("\n");
	
	ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
	size_ip = IP_HL(ip)*4;
	if (size_ip < 20) {
		printf("   * Invalid IP header length: %u bytes\n", size_ip);
		return;
	}
	
	printf("Source IP: ");
	for (i=0; i<18; i++)
	{
		printf("%c", inet_ntoa(ip->ip_src)[i]);
	}
	printf("\n");
	
	
	printf("Destination IP: ");
	for (i=0; i<18; i++)
	{
		printf("%c", inet_ntoa(ip->ip_dst)[i]);
	}
	printf("\n");
	printf("\n");
	/*if (size_tcp < 20) {
		printf("   * Invalid TCP header length: %u bytes\n", size_tcp);
		return;
	}*/

	/*printf("Packet Count: %d\n", ++count);             
	printf("Recieved Packet Size: %d\n", pkthdr->len); 
	printf("Payload:\n");                             
	for(i=0;i<pkthdr->len;i++) { 
		if(isprint(packet[i]))           
			printf("%c ",packet[i]);       
		else 
			printf(" . ",packet[i]);       
		if((i%16==0 && i!=0) || i==pkthdr->len-1) 
			printf("\n"); 
	}*/
}


int main(int argc,char **argv) 
{ 
	int i;
	char *dev; 
	char errbuf[PCAP_ERRBUF_SIZE]; 
	pcap_t* descr; 
	const u_char *packet; 
	struct pcap_pkthdr hdr;
	struct ether_header *eptr; /* net/ethernet.h */ 
	struct bpf_program fp;     /*выражение фильтрации в составленном виде */ 
	bpf_u_int32 maskp;         /*маска подсети */ 
	bpf_u_int32 netp;          /* ip */ 

	if(argc != 2){
		fprintf(stdout, "Usage: %s \"expression\"\n" 
			,argv[0]);
		return 0;
	} 

	/* Получение имени устройства */
	dev = pcap_lookupdev(errbuf); 
	
	if(dev == NULL) {
		fprintf(stderr, "%s\n", errbuf);
		exit(1);
	} 
	/* Получение сетевого адреса и маски сети для устройства */ 
	pcap_lookupnet(dev, &netp, &maskp, errbuf); 

	/* открытие устройства в  promiscuous-режиме */ 
	descr = pcap_open_live(dev, BUFSIZ, 1,-1, errbuf); 
	if(descr == NULL) {
		printf("pcap_open_live(): %s\n", errbuf);
		exit(1);
	} 

	/* теперь составляется выражение фильтрации*/ 
	if(pcap_compile(descr, &fp, argv[1], 0, netp) == -1) {
		fprintf(stderr, "Error calling pcap_compile\n");
		exit(1);
	} 

	/* применение фильтра*/ 
	if(pcap_setfilter(descr, &fp) == -1) {
		fprintf(stderr, "Error setting filter\n");
		exit(1);
	} 

	/* функция обратного вызова используется в цикле */ 
	pcap_loop(descr, -1, another_callback, NULL); 
	return 0; 
}
